package udsu;

import com.sleepycat.persist.model.Persistent;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Persistent
@Entity
public class func_linear {
	@SecondaryKey(relate=Relationship.MANY_TO_ONE, relatedEntity=func_service.class, name="id")
	private int fs_id;
	@SecondaryKey(relate=Relationship.MANY_TO_ONE, relatedEntity=linear_department.class, name="id")
	private int dep_id;
	
	public func_linear(int fs_id, int dep_id) {
       this.fs_id = fs_id;
	   this.dep_id = dep_id;
    }
	
	private func_linear() {
    }
	
	public int getFs_id() {
        return fs_id;
    }
	
	public void setFs_id(int fs_id) {
        this.fs_id = fs_id;
    }
	
	public int getDep_id() {
        return dep_id;
    }
	
	public void setDep_id(int dep_id) {
        this.dep_id = dep_id;
    }
}