package udsu;

import com.sleepycat.persist.model.Persistent;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Persistent
@Entity
public class func_service {
	@PrimaryKey(sequence="ID")
	private int id;
	private String name;
	private String functions;
	private int emp_count;
	@SecondaryKey(relate=Relationship.ONE_TO_ONE, relatedEntity=leaders.class, name="id")
	private int leader_id;
	@SecondaryKey(relate=Relationship.MANY_TO_ONE, relatedEntity=organization.class, name="id")
	private int org_id;
	
	public func_service(int id, String name, String functions, int emp_count, int leader_id, int org_id) {
       this.id = id;
	   this.name = name;
	   this.functions = functions;
	   this.emp_count = emp_count;
	   this.leader_id = leader_id;
	   this.org_id = org_id;
    }
	
	private func_service() {
    }
	
	public int getId() {
        return id;
    }
	
	public void setId(int id) {
        this.id = id;
    }
	
	public String getName() {
        return name;
    }
	
	public void setName(String name) {
        this.name = name;
    }
	
	public String getFunctions() {
        return functions;
    }
	
	public void setFunctions(String functions) {
        this.functions = functions;
    }
	
	public int getEmp_count() {
        return emp_count;
    }
	
	public void setEmp_count(int emp_count) {
        this.emp_count = emp_count;
    }
	
	public int getLeader_id() {
        return leader_id;
    }
	
	public void setLeader_id(int leader_id) {
        this.leader_id = leader_id;
    }
	
	public int getOrg_id() {
        return org_id;
    }
	
	public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }
}
