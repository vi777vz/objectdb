package udsu;

import com.sleepycat.persist.model.Persistent;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import java.util.Date;

@Persistent
@Entity
public class leaders {
	@PrimaryKey(sequence="ID")
	private int id;
	private String name;
	private String surname;
	private String patronymic;
	private Date birthday;
	private String address;
	
	public leaders(int id, String name, String surname, String patronymic, Date birthday, String address) {
       this.id = id;
	   this.name = name;
	   this.surname = surname;
	   this.patronymic = patronymic;
	   this.birthday = birthday;
	   this.address = address;
    }
	
	private leaders() {
    }
	
	public int getId() {
        return id;
    }
	
	public void setId(int id) {
        this.id = id;
    }
	
	public String getName() {
        return name;
    }
	
	public void setName(String name) {
        this.name = name;
    }
	
	public String getSurname() {
        return surname;
    }
	
	public void setSurname(String surname) {
        this.surname = surname;
    }
	
	public String getPatronymic() {
        return patronymic;
    }
	
	public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
	
	public Date getBirthday() {
        return birthday;
    }
	
	public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
	
	public String getAddress() {
        return address;
    }
	
	public void setAddress(String address) {
        this.address = address;
    }
}
