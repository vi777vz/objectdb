package udsu;

import com.sleepycat.persist.model.Persistent;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Persistent
@Entity
public class organization {
	@PrimaryKey(sequence="ID")
	private int id;
	private String name;
	private String addr;
	private String opf;
	private String bank;
	private int inn;
	@SecondaryKey(relate=Relationship.ONE_TO_ONE, relatedEntity=leaders.class, name="id")
	private int leader_id;
	
	public organization(int id, String name, String addr, String opf, String bank, int inn, int leader_id) {
       this.id = id;
	   this.name = name;
	   this.addr = addr;
	   this.opf = opf;
	   this.bank = bank;
	   this.inn = inn;
	   this.leader_id = leader_id;
    }
	
	private organization() {
    }
	
	public int getId() {
        return id;
    }
	
	public void setId(int id) {
        this.id = id;
    }
	
	public String getName() {
        return name;
    }
	
	public void setName(String name) {
        this.name = name;
    }
	
	public String getAddr() {
        return addr;
    }
	
	public void setAddr(String addr) {
        this.addr = addr;
    }
	
	public String getOpf() {
        return opf;
    }
	
	public void setOpf(String opf) {
        this.opf = opf;
    }
	
	public String getBank() {
        return bank;
    }
	
	public void setBank(String bank) {
        this.bank = bank;
    }
	
	public int getInn() {
        return inn;
    }
	
	public void setInn(int inn) {
        this.inn = inn;
    }
	
	public int getLeader_id() {
        return leader_id;
    }
	
	public void setLeader_id(int leader_id) {
        this.leader_id = leader_id;
    }
}
